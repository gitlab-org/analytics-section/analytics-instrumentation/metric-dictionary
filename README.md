# Dictionary of the Service Ping metric definitions

With this dictionary we have an up-to-date collection of
GitLab's Service Ping metrics and Snowplow events available.
If you want to contribute to the project, please submit a
merge request and ping @gitlab-org/analytics-section/product-intelligence. 

The project can be viewed at: https://metrics.gitlab.com

## Development instructions

### Architecture

We query source data via GitLab's GraphQL and REST APIs, parse
the YML files and generate JSON files to be rendered in a
Javascript powered [table](https://gridjs.io/), rendered using [Nuxt](https://nuxtjs.org/).

The pages are rebuilt daily with fresh data at 9:00 AM UTC.

### How to develop locally

1. Get the source code and change directory to the cloned project.

   ```sh
   git clone git@gitlab.com:gitlab-org/analytics-section/analytics-instrumentation/metric-dictionary.git
   
   cd ./metrics-dictionary
   ```

1. Install required dependencies using [asdf](https://asdf-vm.com/guide/getting-started.html) or manually. The dependencies and the required versions can be found in the `.tool-versions` file located at the root folder.

   ```sh
   asdf install
   ```

1. Run the following commands to generate the metric dictionary page.

   ```sh
   bin/seed

   bin/build
   ```

1. Install the required NodeJS dependencies. The Nuxt application is located in the `/web` directory.

   ```sh
   yarn --cwd ./web
   ```

1. Start the development web server.

   ```sh
   yarn --cwd ./web dev
   ```

### Debugging the application

To open the project's console, run:

```sh
bin/console
```

### MR review process

After creating an MR ask for a review in the [#g_monitor_analytics_instrumentation](https://gitlab.slack.com/archives/CL3A7GFPF) channel or ping an [Analytics Instrumentation team member](https://handbook.gitlab.com/handbook/engineering/development/analytics/analytics-instrumentation/#team-members) in your MR or issue. 
