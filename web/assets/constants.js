import ProductGroupToken from '@/components/search/product_group_token.vue'
import ProductCategoryToken from '@/components/search/product_category_token.vue'
import TimeFrameToken from '@/components/search/time_frame_token.vue'
import SourceToken from '@/components/search/source_token.vue'
import MilestoneToken from '@/components/search/milestone_token.vue'
import CategoryToken from '@/components/search/category_token.vue'
import ActionToken from '@/components/search/action_token.vue'
import KeyToken from '@/components/search/key_token.vue'
import StatusToken from '@/components/search/status_token.vue'
import TierToken from '@/components/search/tier_token.vue'
import TypeToken from '@/components/search/type_token.vue'
import ApplicationToken from '~/components/search/application_token.vue'
import PerformanceToken from '~/components/search/performance_token.vue'

export const METRIC = 'metric'
export const EVENT = 'event'
export const DASHBOARD = 'dashboard'

export const PERFORMANCE_INDICATOR_TYPE = 'performance_indicator_type'

export const DATA_MAPPER = {
  [EVENT]: {
    productGroup: 'product_group',
    productCategory: 'product_categories',
    timeframe: 'time_frame',
    sources: 'data_source',
    milestone: 'milestone',
    category: 'category',
    action: 'action',
    key: 'key',
    tiers: 'tiers',
    serviceName: 'service_name',
  },
  [METRIC]: {
    productGroup: 'product_group',
    productCategory: 'product_categories',
    performanceIndicatorType: PERFORMANCE_INDICATOR_TYPE,
    timeframe: 'time_frame',
    sources: 'data_source',
    milestone: 'milestone',
    category: 'data_category',
    action: 'action',
    key: 'key',
    status: 'status',
    tiers: 'tiers',
  },
  [DASHBOARD]: {
    productGroup: 'product_group',
    productCategory: 'product_categories',
    serviceName: 'service_name',
  },
}

export const AVAILABLE_FILTERS = {
  [EVENT]: [
    'serviceName',
    'productGroup',
    'productCategory',
    'category',
    'action',
    'key',
    'milestone',
    'tiers',
  ],
  [METRIC]: [
    'productGroup',
    'productCategory',
    'timeframe',
    'sources',
    'milestone',
    'category',
    'status',
    'tiers',
    'performanceIndicatorType',
  ],
  [DASHBOARD]: ['type', 'productGroup', 'productCategory', 'serviceName'],
}

export const FIELDS_FOR_TEXT_SEARCH = {
  [EVENT]: [
    'service_name',
    'action',
    'key',
    'description',
    'label_description',
    'property_description',
    'value_description',
    'extra_properties',
    'identifiers',
  ],
  [METRIC]: [
    'instrumentation_class',
    'key_path',
    'description',
    'value_type',
    'options',
    'events',
    PERFORMANCE_INDICATOR_TYPE,
  ],
  [DASHBOARD]: ['type', 'productGroup'],
}

export const OPERATOR_IS = '='
export const OPERATOR_IS_TEXT = 'is'

export const OPERATORS_IS = [
  { value: OPERATOR_IS, description: OPERATOR_IS_TEXT },
]

export const CONTEXT_TOKEN_HASH = {
  [METRIC]: [
    {
      type: 'productGroup',
      title: 'Product Group',
      unique: true,
      token: ProductGroupToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'productCategory',
      title: 'Product Category',
      unique: true,
      token: ProductCategoryToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'performanceIndicatorType',
      title: 'Performance Indicator Type',
      unique: true,
      token: PerformanceToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'timeframe',
      unique: true,
      title: 'Timeframe',
      token: TimeFrameToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'sources',
      unique: true,
      title: 'Sources',
      token: SourceToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'milestone',
      unique: true,
      title: 'Milestone',
      token: MilestoneToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'category',
      title: 'Category',
      unique: true,
      token: CategoryToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'status',
      title: 'Status',
      unique: true,
      token: StatusToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'tiers',
      title: 'Tiers',
      unique: true,
      token: TierToken,
      operators: OPERATORS_IS,
    },
  ],
  [EVENT]: [
    {
      type: 'serviceName',
      title: 'Service Name',
      unique: true,
      token: ApplicationToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'productGroup',
      title: 'Product Group',
      unique: true,
      token: ProductGroupToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'productCategory',
      title: 'Product Category',
      unique: true,
      token: ProductCategoryToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'category',
      title: 'Category',
      unique: true,
      token: CategoryToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'action',
      title: 'Action',
      unique: true,
      token: ActionToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'key',
      title: 'Key',
      unique: true,
      token: KeyToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'milestone',
      unique: true,
      title: 'Milestone',
      token: MilestoneToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'tiers',
      title: 'Tiers',
      unique: true,
      token: TierToken,
      operators: OPERATORS_IS,
    },
  ],
  [DASHBOARD]: [
    {
      type: 'serviceName',
      title: 'Service Name',
      unique: true,
      token: ApplicationToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'type',
      title: 'Type',
      unique: true,
      token: TypeToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'productGroup',
      title: 'Product Group',
      unique: true,
      token: ProductGroupToken,
      operators: OPERATORS_IS,
    },
    {
      type: 'productCategory',
      title: 'Product Category',
      unique: true,
      token: ProductCategoryToken,
      operators: OPERATORS_IS,
    },
  ],
}
