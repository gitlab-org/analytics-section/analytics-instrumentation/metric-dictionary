export default function getGitLabUrl(path = '') {
  return new URL(path, 'https://gitlab.com').toString()
}
