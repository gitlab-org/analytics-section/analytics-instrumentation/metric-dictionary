# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/event_row"
require "json"

RSpec.describe EventRow do
  subject(:row) { described_class.new(data) }
  let(:tiers) { ["free", "premium", "ultimate"] }

  let(:data) do
    {
      "id"=> "gid://gitlab/Blob/834d13968d0fd15da47e675937083218294d5b9f",
      "name"=> "1643968255_projectsnew_select_deployment_target.yml",
      "webPath"=> "/gitlab-org/gitlab/-/blob/master/config/events/1643968255_projectsnew_select_deployment_target.yml",
      "rawBlob"=> {
        "description"=> "Deployment target option selected from new project creation form",
        "category"=> "projects:new",
        "action"=> "select_deployment_target",
        "label_description"=> "new_project_deployment_target",
        "property_description"=> "selected option (string)",
        "product_group"=> "configure",
        "product_categories" => ["deployment_management"],
        "milestone"=> "14.8",
        "introduced_by_url"=> "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79873",
        "tiers"=> tiers
      }
    }
  end

  it "returns definition basename as key" do
    expect(row.key).to eq("1643968255_projectsnew_select_deployment_target")
  end

  describe "#tiers" do
    subject { row.tiers }

    context "with all tiers" do
      let(:tiers) { ["free", "premium", "ultimate"] }

      it "returns all tiers" do
        is_expected.to eq(["free", "premium", "ultimate"])
      end
    end

    context "with premium and ultimate" do
      let(:tiers) { ["premium", "ultimate"] }

      it "returns both tiers" do
        is_expected.to eq(["premium", "ultimate"])
      end
    end

    context "with ultimate" do
      let(:tiers) { ["ultimate"] }

      it { is_expected.to eq(["ultimate"]) }
    end

    context "with no tiers" do
      let(:tiers) { [] }

      it { is_expected.to eq([]) }
    end
  end

  describe "#category" do
    subject { row.category }

    context 'without internal_events' do
      it { is_expected.to eq("projects:new") }
    end

    context 'without category' do
      before do
        data["rawBlob"].delete('category')
      end

      context 'when not internal_events' do
        it { is_expected.to be_nil }
      end

      context 'with internal_events set' do
        before do
          data["rawBlob"]["internal_events"] = true
        end

        it {is_expected.to eq("InternalEventsTracking") }
      end
    end

  end

  describe "#web_path" do
    subject { row.web_path }

    it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/events/1643968255_projectsnew_select_deployment_target.yml") }
  end
end
