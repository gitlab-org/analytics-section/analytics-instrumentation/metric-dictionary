import * as echarts from 'echarts'
import { getSearchQuery } from '@/assets/get-search-query'
import { extractSearchValues } from '@/assets/utils'

export const chartMixin = {
  data() {
    return {
      milestoneBarChart: null,
      tierBarChart: null,
      productCategoriesChart: null,
    }
  },
  methods: {
    initializeCharts(chartRefs, basePath) {
      chartRefs.forEach((chartRef) => {
        this[chartRef] = echarts.init(this.$refs[chartRef])
        this[chartRef].on('click', (params) =>
          this.handleChartClick(params, basePath),
        )
      })

      if (basePath === '/') {
        this.updateSummary()
      }
      this.updateCharts()
    },
    resizeCharts(chartRefs) {
      chartRefs.forEach((chartRef) => {
        this[chartRef] && this[chartRef].resize()
      })
    },
    disposeCharts(chartRefs) {
      chartRefs.forEach((chartRef) => {
        this[chartRef] && this[chartRef].dispose()
      })
    },
    handleChartClick(params, basePath) {
      const { seriesName, name } = params

      // Filters currently support only `IS` matching, not `NOT`
      if (name.includes('Unknown')) return

      const searchValues = extractSearchValues(getSearchQuery())
      const queryParams = new URLSearchParams()
      queryParams.append(seriesName, name)
      Object.keys(searchValues).forEach((key) => {
        if (
          key !== 'type' &&
          searchValues[key] &&
          searchValues[key].length > 0
        ) {
          searchValues[key].forEach((value) => {
            queryParams.append(key, value)
          })
        }
      })

      this.$router.push(`${basePath}?${queryParams.toString()}`)
    },
    updateMileStoneChart() {
      const milestoneData = this.getMilestoneData()

      this.milestoneBarChart?.setOption({
        tooltip: {
          trigger: 'axis',
          axisPointer: { type: 'shadow' },
          formatter: '{b}: {c}',
        },
        xAxis: {
          type: 'category',
          data: milestoneData.categories,
          axisLabel: {
            rotate: 45,
            interval: 0,
          },
        },
        yAxis: {
          type: 'value',
          name: 'Count',
        },
        series: [
          {
            name: 'milestone',
            type: 'bar',
            data: milestoneData.values,
            itemStyle: {
              color: function (params) {
                const colorList = [
                  '#e6194b',
                  '#3cb44b',
                  '#ffe119',
                  '#0082c8',
                  '#f58231',
                  '#911eb4',
                  '#46f0f0',
                  '#f032e6',
                  '#d2f53c',
                  '#fabebe',
                  '#008080',
                  '#e6beff',
                  '#aa6e28',
                  '#fffac8',
                  '#800000',
                  '#aaffc3',
                  '#808000',
                  '#ffd8b1',
                  '#000080',
                  '#808080',
                ]
                return colorList[params.dataIndex % colorList.length]
              },
            },
          },
        ],

        dataZoom: [
          {
            type: 'slider',
            show: true,
            start: 80,
            end: 100,
            xAxisIndex: [0],
          },
        ],
      })
    },
    updateTierChart() {
      const tierData = this.getTierData()
      this.tierBarChart?.setOption({
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow',
          },
          formatter: '{b0}: {c0}',
        },
        xAxis: {
          type: 'category',
          data: ['free', 'premium', 'ultimate'],
        },
        yAxis: {
          type: 'value',
        },
        series: [
          {
            name: 'tiers',
            type: 'bar',
            data: tierData,
            itemStyle: {
              color: function (params) {
                const colorList = ['#28a745', '#dc3545', '#ffc107']
                return colorList[params.dataIndex]
              },
            },
          },
        ],
      })
    },
    updateProductCategoriesChart() {
      const productCategoryData = this.getProductCategoryData()

      this.productCategoriesChart?.setOption({
        tooltip: {
          trigger: 'axis',
          axisPointer: { type: 'shadow' },
          formatter: '{b}: {c}',
        },
        yAxis: {
          type: 'category',
          data: productCategoryData.map(({ name }) => name),
          axisLabel: {
            show: false,
          },
        },
        grid: { containLabel: true },
        xAxis: {
          type: 'value',
          name: 'Count',
        },
        series: [
          {
            name: 'productCategory',
            type: 'bar',
            data: productCategoryData.map(({ value }) => value),
            itemStyle: {
              normal: {
                label: {
                  show: true,
                  position: 'outside',
                  formatter: ({ name }) => name,
                },
              },
            },
          },
        ],
      })
    },
    getMilestoneData() {
      const milestones = this.rows.reduce((acc, row) => {
        const milestone = row.milestone || 'Unknown Milestone'
        acc[milestone] = (acc[milestone] || 0) + 1
        return acc
      }, {})

      const milestoneArray = Object.entries(milestones).sort((a, b) =>
        a[0].localeCompare(b[0], undefined, { numeric: true }),
      )

      const sortedCategories = milestoneArray.map((item) => item[0])
      const sortedValues = milestoneArray.map((item) => item[1])

      return {
        categories: sortedCategories,
        values: sortedValues,
      }
    },
    getTierData() {
      const tierCounts = { free: 0, premium: 0, ultimate: 0 }
      this.rows.forEach((row) => {
        const tiers = Array.isArray(row.tiers) ? row.tiers : []
        tiers.forEach((tier) => {
          if (tierCounts[tier.toLowerCase()] !== undefined) {
            tierCounts[tier.toLowerCase()] += 1
          }
        })
      })

      return [tierCounts.free, tierCounts.premium, tierCounts.ultimate]
    },
    getProductCategoryData() {
      let data = {}
      let countUnknown = 0

      this.rows.forEach((row) => {
        const hasCategories =
          Array.isArray(row.product_categories) && row.product_categories.length

        if (hasCategories) {
          row.product_categories.forEach((category) => {
            data[category] = (data[category] || 0) + 1
          })
        } else {
          countUnknown = countUnknown + 1
        }
      })

      data = Object.entries(data)
        .map(([name, value]) => ({ name, value }))
        .sort((labelA, labelB) => labelB.name.localeCompare(labelA.name))
      data.unshift({ name: 'Unknown', value: countUnknown })

      return data
    },
    getCategoryData() {
      const categories = this.rows.reduce((acc, row) => {
        const category = row.data_category || 'Unknown Category'
        acc[category] = (acc[category] || 0) + 1
        return acc
      }, {})

      return Object.entries(categories).map(([name, value]) => ({
        name,
        value,
      }))
    },
    getSourceData() {
      const categories = this.rows.reduce((acc, row) => {
        const category = row.data_source || 'Unknown source'
        acc[category] = (acc[category] || 0) + 1
        return acc
      }, {})

      return Object.entries(categories).map(([name, value]) => ({
        name,
        value,
      }))
    },
  },
}
