import { isEmpty } from 'lodash'
import compare from 'string-comparison'

import {
  DATA_MAPPER,
  METRIC,
  AVAILABLE_FILTERS,
  FIELDS_FOR_TEXT_SEARCH,
  PERFORMANCE_INDICATOR_TYPE,
} from '@/assets/constants'
import { setSearchQuery } from '@/assets/get-search-query'

export const getTableauUrl = (path) => {
  return `https://10az.online.tableau.com/#/site/gitlab/views/PDServicePingExplorationDashboard/MetricTrend?Metrics%20Path=${path}`
}

export const getReportIssueUrl = (context, path) => {
  return `https://gitlab.com/gitlab-org/analytics-section/analytics-instrumentation/metric-dictionary/-/issues/new?issue[title]=Problem+with+${context}%3A%20%60${path}%60`
}

export function extractSearchValues(tokens = []) {
  return tokens.reduce((acc, searchFilter) => {
    if (typeof searchFilter === 'string' && searchFilter.length >= 3) {
      acc.searchText = [searchFilter]
    } else if (searchFilter?.type) {
      const type = searchFilter.type
      const value = searchFilter?.value?.data

      if (value) {
        if (!acc[type]) {
          acc[type] = []
        }
        acc[type].push(value)
      }
    }
    return acc
  }, {})
}

function matchesSearchText(searchValues, dataRow, context) {
  const searchText = searchValues?.searchText?.[0]
  if (!searchText) return true

  return Object.keys(dataRow).some((key) => {
    if (!FIELDS_FOR_TEXT_SEARCH[context].includes(key)) return false

    const field = dataRow[key]
    if (!field) return false

    if (Array.isArray(field)) {
      return field.some((text) => text && isTextSimilar(text, searchText, {}))
    } else {
      return isTextSimilar(field, searchText, {})
    }
  })
}

function isTextSimilar(
  baseText = '',
  textToMatch,
  { similarityThreshold = 1 },
) {
  if (typeof baseText === 'string' || baseText instanceof String) {
    baseText = baseText.toLowerCase()
    textToMatch = textToMatch.toLowerCase()

    const directMatch = baseText.includes(textToMatch)
    const similarityMatch =
      compare.jaroWinkler.similarity(baseText, textToMatch) >=
      similarityThreshold

    return directMatch || similarityMatch
  }
}

function matchesOtherFilters(searchValues, dataRow, context) {
  return AVAILABLE_FILTERS[context].every((filterType) => {
    if (filterType === 'type') return true
    const filterValues = searchValues[filterType]
    if (!filterValues) {
      return true
    }

    const field = dataRow[DATA_MAPPER[context][filterType]]

    if (Array.isArray(field)) {
      return field.some((element) => filterValues.includes(element))
    } else {
      if (filterType === 'performanceIndicatorType') {
        return field?.includes(filterValues)
      }
      return filterValues?.includes(field)
    }
  })
}

export function fetchSearchResults(tokens, fetchedData, context = METRIC) {
  setSearchQuery({})

  if (isEmpty(tokens)) {
    return fetchedData
  }

  const searchValues = extractSearchValues(tokens)
  setSearchQuery(searchValues)

  if (searchValues.type && Object.keys(searchValues).length === 1) {
    return fetchedData
  }

  return fetchedData.filter((dataRow) => {
    const textMatch = matchesSearchText(searchValues, dataRow, context)
    const filterMatch = matchesOtherFilters(searchValues, dataRow, context)

    return textMatch && filterMatch
  })
}

const convertMilestoneValue = (value) => {
  if (typeof value === 'string' && value?.startsWith('<')) {
    return parseFloat(value.substring(1))
  }
  return parseFloat(value)
}

export function extractUniqueValues(fetchedData, key, context = null) {
  if (key === 'milestone') {
    const uniqueMilestones = [
      ...new Set(fetchedData.map((item) => item.milestone)),
    ]

    return uniqueMilestones
      ?.map((milestone) =>
        milestone?.startsWith('<') ? `<${milestone.substring(1)}` : milestone,
      )
      .sort((a, b) => convertMilestoneValue(a) - convertMilestoneValue(b))
  }

  if (key === 'tiers') {
    return ['free', 'premium', 'ultimate']
  }

  if (key === 'performanceIndicatorType') {
    const perfTypes = [
      ...new Set(fetchedData.map((item) => item[PERFORMANCE_INDICATOR_TYPE])),
    ]

    return [
      ...new Set(
        perfTypes
          .filter((item) => item !== '')
          .flatMap((item) => item.split(',').map((str) => str.trim()))
          .filter(Boolean),
      ),
    ]
  }

  if (key === 'category') {
    return [
      ...new Set(
        fetchedData.map((item) =>
          context === METRIC ? item.data_category : item.category,
        ),
      ),
    ]
  }

  if (key === 'type') return ['metrics', 'events']
  if (key === 'key') {
    return fetchedData.map((item) => item.key)
  }
  if (key === 'productCategory') {
    return [...new Set(fetchedData.flatMap((item) => item.product_categories))]
  }
  return [
    ...new Set(fetchedData.map((item) => item[DATA_MAPPER[context][key]])),
  ]
}
