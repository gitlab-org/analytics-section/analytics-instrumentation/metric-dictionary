export function getSearchQuery() {
  const params = new URLSearchParams(window.location.search)
  const output = []

  params.forEach((value, key) => {
    if (value.includes(',')) {
      const values = value.split(',')
      values.forEach((val, index) => {
        output.push({
          type: key,
          value: {
            data: val,
            operator: '=',
          },
          id: `${key}-token-${index + 1}`,
        })
      })
    } else {
      output.push({
        type: key,
        value: {
          data: value,
          operator: '=',
        },
        id: `${key}-token-1`,
      })
    }
  })

  return output
}

function buildQueryString(searchParams) {
  const params = new URLSearchParams()

  Object.entries(searchParams).forEach(([key, value]) => {
    if (Array.isArray(value)) {
      params.set(key, value.join(','))
    } else {
      params.set(key, value)
    }
  })

  return params.toString() ? `?${params.toString()}` : ''
}

export function setSearchQuery(searchParams) {
  const queryString = buildQueryString(searchParams)

  window.history.replaceState(
    {},
    document.title,
    `${window.location.pathname}${queryString}`,
  )
}
