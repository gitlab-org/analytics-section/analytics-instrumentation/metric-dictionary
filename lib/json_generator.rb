# frozen_string_literal: true

require 'json'
require 'digest/md5'
require_relative 'metrics_loader'
require_relative 'events_loader'
require_relative 'parser'
require_relative 'metric_row'
require_relative 'event_row'

class JsonGenerator
  def initialize
    @root_path = 'web/static/data'
    @snowplow_data = generate_snowplow_data
    @service_ping_data = generate_service_ping_data
    @hashes = {}
  end

  def generate
    write_json(:service_ping)
    write_json(:snowplow)
    update_manifest
  end

  private

  attr_reader :snowplow_data, :service_ping_data

  def data(data_type)
    data =
      if data_type == :service_ping
        service_ping_data.map { |metric| MetricRow.new(metric) }
      else
        snowplow_data.map { |event| EventRow.new(event) }
      end

    json_payload = data.to_json
    @hashes[data_type] = Digest::MD5.hexdigest(json_payload)

    {
      data: json_payload,
      hash: @hashes[data_type],
      type: data_type.to_s,
      filename: File.join(@root_path, "#{data_type}.json")
    }
  end

  def update_manifest
    manifest_path = File.join(@root_path, 'manifest.json')
    manifest_data = @hashes.to_json

    File.open(manifest_path, "w") do |f|
      f.write(manifest_data)
    end
  end

  def write_json(data_type)
    json_data = data(data_type)

    return if json_data[:data].empty?

    # Remove previous versions of the data
    files_pattern = File.join(@root_path, "#{json_data[:type]}.*")
    Dir.glob(files_pattern).each { |file| File.delete(file) }

    json_filename = json_data[:filename].sub('.json', ".#{json_data[:hash]}.json")

    File.open(json_filename, "w") do |f|
      f.write(json_data[:data])
    end
  end

  def generate_snowplow_data
    loader = EventsLoader.new

    Parser.new(fetch_loader_data(loader)).parse
  end

  def generate_service_ping_data
    loader = MetricsLoader.new

    Parser.new(fetch_loader_data(loader)).parse
  end

  def fetch_loader_data(loader)
    return loader.load if loader.fetched?

    data = loader.fetch
    loader.save
    data
  end
end
